﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            for (int i = 0; i < 5; i++)
            {
                Console.Write("Please enter your " + (i + 1) + " number: ");

                var n = Console.ReadLine();
                var num = 0;
                bool valid = int.TryParse(n, out num);  // check for non-numerical input

                while (!valid)                          //loop until the user inputs a number
                {
                    Console.WriteLine("Invalid input! Please try again!");
                    n = string.Empty;                   // empties the invalid input

                    Console.WriteLine();                //  new line
                    Console.Write("Please enter your " + (i + 1) + " number: ");
                    n = Console.ReadLine();
                    valid = int.TryParse(n,out num);
                }                
                sum += num;                
            }
            Console.WriteLine("The sum of these five numbers are : " + sum + "\n"); //outputs the result
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
