﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32
{
    class Program
    {
        public static string AddOrdinal(int num)
        {
            if (num <= 0)
            return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }

        }

        static void Main(string[] args)
        {
            string[] days = new string[] { "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" };
            while(true)
            {
                Console.Write("Please enter a day in the week: ");
                string myDay = Console.ReadLine().ToLower();
                int count = 0;
                foreach (var item in days)
                {
                    if(item != myDay)
                    {
                        count++;
                    }
                }
                if(count == days.Length)
                {
                    Console.WriteLine("Invalid Input! Please Try Again!\n");
                    myDay = string.Empty;
                }   
                else
                {
                    var MyIndex = (Array.IndexOf(days, myDay) + 1);
                    var index = AddOrdinal(MyIndex);
                    Console.WriteLine();
                    Console.WriteLine(char.ToUpper(myDay[0]) + myDay.Substring(1) + " is the " + index + " day of the week.\n");
                    break;
                }              
            }
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
