﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Write("Please enter the size of your computer in TB: ");
                string UserInput = Console.ReadLine();
                int input;
                bool Valid = int.TryParse(UserInput, out input);
                if(!Valid)
                {
                    Console.WriteLine("Invalid Input! Please Try Again!\n");
                    UserInput = string.Empty;
                }
                else
                {
                    Console.WriteLine("\n" + input + " TB is equal to " + (input * 1024) + " GB.");
                    break;
                }
            } while (true);

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
