﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;

            do
            {
                //the front page
                Console.Clear();

                Console.WriteLine($"******************************");
                Console.WriteLine($"          MAIN MENU           ");
                Console.WriteLine($"                              ");
                Console.WriteLine($"    1. Convert KM to Miles    ");
                Console.WriteLine($"    2. Convert Miles to KM    ");
                Console.WriteLine($"                              ");
                Console.WriteLine($"******************************");
                Console.WriteLine();
                Console.WriteLine($"Please select an option");

                //User Input
                input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        Console.Clear();

                        Console.WriteLine($"******************************");
                        Console.WriteLine($"          Menu 01             ");
                        Console.WriteLine($"                              ");
                        Console.WriteLine($"   To convert KM to Miles");
                        Console.WriteLine($"           ... ...                ");
                        Console.WriteLine($"           ... ...                ");
                        Console.WriteLine($"To go back to the main menu please enter \" M \"");
                        Console.WriteLine($"                              ");
                        Console.WriteLine($"******************************");
                        Console.WriteLine();
                        Console.WriteLine($"Please select an option");
                        Console.WriteLine("Press enter key to quit the program.");

                        input = Console.ReadLine().ToLower();
                        break;

                    case "2":
                        Console.Clear();

                        Console.WriteLine($"******************************");
                        Console.WriteLine($"          Menu 02             ");
                        Console.WriteLine($"                              ");
                        Console.WriteLine($"   To Convert Miles to KM");
                        Console.WriteLine($"           ... ...                ");
                        Console.WriteLine($"           ... ...                ");
                        Console.WriteLine($"To go back to the main menu please enter \" M \"");
                        Console.WriteLine($"                              ");
                        Console.WriteLine($"******************************");
                        Console.WriteLine();
                        Console.WriteLine($"Please select an option");
                        Console.WriteLine("Press enter key to quit the program.");

                        input = Console.ReadLine().ToLower();
                        break;

                    case "m":
                        Console.Clear();
                        break;

                    default:
                        input = "m";
                        break;
                }

            } while (input == "m");

            Console.ReadLine();
            
        }
    }
}
