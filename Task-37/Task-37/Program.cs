﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            int hpc = 10; //10 hours per credit
            int credit = 15;
            Console.WriteLine("Each credit is worthing " + hpc + " hours of studying.\n");
            int ContHour = 5; //5 hours per week in class
            Console.WriteLine("There are " + ContHour + " hours in lectures and tutorials.\n");
            int semeWeek = 12;
            Console.WriteLine("There are " + semeWeek + " weeks in this semester.\n");
            double TotalCont = semeWeek * ContHour;
            Console.WriteLine("We have got " + TotalCont + " Contact Hours in total in this semeter.\n");
            double exHour = credit * hpc - TotalCont;
            Console.WriteLine("Given a semester is 12 weeks long, we need to spend " + exHour + " hours outside class for a 15-credit paper.\n");
            double hour = exHour / semeWeek;
            Console.WriteLine("We need to spend " + hour + " hours outside of class each week.\n");
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();         
        }
    }
}
