﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
    class person
    {
        public string name;
        public int age;
    }

    class Program
    {
        static void Main(string[] args)
        {

            List<person> people = new List<person>();
            
            for (int i = 0; i < 3; i++)
            {
                
                while (true)
                {
                    Console.Write("Please enter name " + (i + 1) + " : ");
                    var name = Console.ReadLine();
                    bool isNull = string.IsNullOrWhiteSpace(name);
                    if(isNull)
                    {
                        Console.WriteLine("Invalid Name Input! Please Try Again!\n");
                        name = string.Empty;
                    }
                    else
                    {
                        
                        while (true)
                        {
                            Console.Write("Please enter " + name + "'s age: ");
                            var input = Console.ReadLine();
                            Console.WriteLine();
                            int age;
                            bool valid = int.TryParse(input, out age);
                            if (valid)
                            {
                                var newperson = new person();

                                newperson.name = name;
                                newperson.age = age;
                                people.Add(newperson);

                                break;
                            }
                            else
                            {
                                Console.WriteLine("Invalid age input for " + name + "! Please try again");
                                input = string.Empty;
                            }
                        }
                        break;
                    }
                }              
              
            }

            foreach (var item in people)
            {
                Console.WriteLine( item.name + "'s age is " + item.age + ".");
            }
                             
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
   
}
