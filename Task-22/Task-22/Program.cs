﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> FruitVege = new Dictionary<string, string>();
            FruitVege.Add("Apple", "Fruit");
            FruitVege.Add("Carrot", "Vegetable");
            FruitVege.Add("Banana", "Fruit");
            FruitVege.Add("Radish", "Vegetable");
            FruitVege.Add("Pear", "Fruit");
            int FruitCount = 0;
            int VegeCount = 0;

            foreach (var item in FruitVege)
            {
                if (item.Value == "Fruit")
                {
                    Console.WriteLine(item.Key + " is a fruit.");
                    FruitCount++;
                }
                else if (item.Value == "Vegetable")
                {
                    VegeCount++;
                }
            }
            Console.WriteLine("\nThere are " + FruitCount + " fruits and " + VegeCount + " vegetables in the list.");
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
