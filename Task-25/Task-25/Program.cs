﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.Write("Please enter a number: ");
                var input = Console.ReadLine();
                int i;
                bool IsValid = int.TryParse(input, out i);
                if(!IsValid)
                {
                    Console.WriteLine("Invalid Input! Please Try Again!\n");
                    input = String.Empty;
                }
                else
                {
                    Console.WriteLine("You have entered Number \"" + i + "\"");
                    break;
                }
            }

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
