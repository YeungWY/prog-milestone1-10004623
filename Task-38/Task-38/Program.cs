﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.Write("Please enter a number: ");                
                string input = Console.ReadLine();
                double i;
                bool IsNumber = double.TryParse(input, out i);
                if (!IsNumber || i == 0 )
                {
                    Console.WriteLine("Invalid Input! Please Try Again!\n");
                    input = string.Empty;
                }
                else
                {
                    for (int x = 1; x <= 12; x++)
                    {
                        double result;
                        result = Math.Round(x / i, 2);
                        Console.WriteLine(x + " ÷ " + i + " = " + result);
                        Console.WriteLine();
                    }
                    break;
                }
            }
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();            
        }
    }
}
