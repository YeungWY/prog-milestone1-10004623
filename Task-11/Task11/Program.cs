﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter a year: ");

            string input = Console.ReadLine();
            int y = 0 ;
            bool year = int.TryParse(input, out y);   //check for non-numerical input
            int length = y.ToString().Length;     //count the length of the input

            while (!year || length != 4)    //loop untils the user inputs a 4-digit number
            {
                input = string.Empty;    //clear the string

                Console.WriteLine("Invalid Year input!");
                Console.Write("\nPlease enter a year: ");
                input = Console.ReadLine();
                year = int.TryParse(input, out y);
                length = y.ToString().Length;    //re-count the length of the string
            }

            bool isLeapYear = DateTime.IsLeapYear(y);   //check if it is a leap year

            if(!isLeapYear)                              // outputs the result
            {
                Console.WriteLine(y + " is not a leap year.");
            }
            else
            {
                Console.WriteLine(y + " is a leap year.");
            }

            Console.WriteLine("\nPress any key to continue..."); 
            Console.ReadKey();
        }

    }
}
