﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class person
    {
        public string Name;
        public DateTime Birthday;
    }
    class Program
    {
        static string GetStringInput(string caption)
        {
            Console.Write(caption);
            var StringInput = Console.ReadLine();

            while (true)
            {
                if (string.IsNullOrWhiteSpace(StringInput))
                {
                    Console.WriteLine("Invalid Name Input, Please Try Again!\n");
                    StringInput = string.Empty;
                    Console.Write(caption);
                    StringInput = Console.ReadLine();
                }
                else
                {
                    break;
                }
            }            
            return StringInput;
        }

        static DateTime GetDateInput(string caption)
        {
            DateTime myDate;
            while (true)
            {
                Console.Write(caption);
                var StringInput = Console.ReadLine();
                bool IsDate = DateTime.TryParse(StringInput, out myDate);
                if (!IsDate)
                {
                    Console.WriteLine("Invalid Date Input, Please Try Again!");
                }
                else
                {
                    break;
                }
            }            
            return myDate;
        }

        static void Main(string[] args)
        {
            List<person> people = new List<person>();
            for (int i = 0; i < 3; i++)
            {
                var myName = GetStringInput("Please enter name" + (i+1) + " : ");
                var myBirthday = GetDateInput("\nPlease enter " + myName +" birthday (dd/mm/yyyy) : ");
                var myPerson = new person();
                myPerson.Name = myName;
                myPerson.Birthday = myBirthday;
                people.Add(myPerson);
                Console.WriteLine();
            }    
                    
            foreach(var item in people)
            {
                Console.WriteLine();
                Console.WriteLine(item.Name + " was born in the " + item.Birthday.ToString("MMMM dd yyyy") + ".");
            }
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }
    }
}
