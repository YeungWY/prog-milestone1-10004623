﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] evenNum = new int[0];
            
            int[] myArray = new int[] { 34, 45, 21, 44, 67, 88, 86 };
            foreach(int item in myArray)
            {
                if (item % 2 == 0)
                {
                    Array.Resize<int>(ref evenNum, evenNum.Length + 1);
                    evenNum[evenNum.Length - 1] = item;
                }
            }

            Console.WriteLine("In the list of numbers: 34, 45, 21, 44, 67, 88, 86\n");
            Console.Write("The even numbers are: ");

            foreach (var item in evenNum)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
