﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_28
{
    class Program
    {
        static void Main(string[] args)
        {  
            while (true)
            {
                Console.Write("Please enter three words seperated by space: ");
                var UserInput = Console.ReadLine();
                string[] words = UserInput.Split(' ');

                int CountArray;
                CountArray = words.Length;
                if(CountArray != 3)
                {
                    Console.WriteLine("Invalid Inputs! Please Try Again!\n");
                    UserInput = string.Empty;
                }
                else
                {
                    foreach (var item in words)
                    {
                        Console.WriteLine(item + "\n");
                    }
                    break;
                }
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
