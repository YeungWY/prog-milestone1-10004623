﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {            
            while(true)
            {
                Console.Write("Please enter a number: ");
                string input = Console.ReadLine();
                int i;
                bool IsNumber = int.TryParse(input, out i);
                if(!IsNumber)
                {
                    Console.WriteLine("Invalid Number Input! Please Try Again!\n");
                    input = string.Empty;
                }
                else
                {
                    if(i % 3 == 0 && i % 4 == 0)
                    {
                        Console.WriteLine();
                        Console.WriteLine(i + " is a exact devision of number 3 and 4.");
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine(i + " is not a exact devision of number 3 and 4.");
                    }
                    break;
                }
            }
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
