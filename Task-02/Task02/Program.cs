﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
    class Program
    {
        public static string AddOrdinal(int num)
        {
            if (num <= 0)
                return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }

        }

        static void Main(string[] args)
        {
            bool invalid = true;    //check for invalid input of month
            while (invalid)         //loop for re-input month
            {
                var monthName = string.Empty;    

                Console.Write("Please enter the MONTH of your Birth: ");
                string mon = Console.ReadLine();
                int month = 0;
                bool valid = int.TryParse(mon,out month);   //check for non-numerical input

                if (!valid || (month > 12 || month < 1))
                {
                    Console.WriteLine("Invalid Month input, please try again!");
                    Console.WriteLine();
                }
                else
                {
                    monthName = new DateTime(2016, month, 1).ToString("MMMM");    // converts int into string and outputs the name of the month
                    invalid = false;
                }                 

                if (invalid == false)
                {
                    bool dateInput = false; //check for invalid date input

                    while (dateInput == false)  //loop for re-enter date
                    {
                        Console.WriteLine();    //empty line
                        Console.Write("Please enter the DATE of your Birth: ");
                        var _date = int.Parse(Console.ReadLine());
                        string mydate = AddOrdinal(_date);
                       
                        if (month < 1 || month > 12 || _date > 31 || _date < 1)
                        {
                            Console.WriteLine("Invalid Date input, please try again!");
                        }

                        else if (_date == 31)
                        {               
                            if (month == 4 || month == 6 || month == 9 || month == 11)
                            {
                                Console.WriteLine("Invalid Date input, please try again!");
                            }
                            else
                            {
                                Console.WriteLine();
                                Console.WriteLine("You were born on the " + monthName + " " + mydate);     
                                dateInput = true;
                            }                          
                        }
                        
                        else if (month == 2 && _date > 29)     //assuming the year is a leap year
                        {
                            Console.WriteLine("Invalid Date input, please try again!");
                        }

                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("You were born on the " + monthName + " " + mydate);
                            dateInput = true;
                        }                      
                    }

                }
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();

        }
    }
}
