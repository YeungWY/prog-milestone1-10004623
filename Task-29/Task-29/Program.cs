﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = "The quick brown fox jumped over the fence.";
            string[] words = input.Split(' ');
            int count = words.Length;
            Console.WriteLine(input);

            Console.WriteLine("\nThere are " + count + " words in this sentence.");
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
