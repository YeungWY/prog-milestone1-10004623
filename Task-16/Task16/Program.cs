﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task16
{
    class Program
    {
        static void Main(string[] args)
        {
            bool invalid = true;       //check for invalid input
            while (invalid)
            {
                string input;
                Console.Write("Please enter a word: ");
                input = Console.ReadLine();

                bool IsAllLetters = true;             //check for non-letters input
                foreach(char i in input)
                {
                    if (!char.IsLetter(i))
                    {
                        IsAllLetters = false;         //if find any non-letters in the string, break
                        break;
                    }
                    else
                        IsAllLetters = true;
                }                    

                if (string.IsNullOrWhiteSpace(input) || IsAllLetters == false)  // check for empty or non-letters string
                {
                    Console.WriteLine("Invalid Input!\n");
                }
                else              //valid input, outputs the result
                {
                    int num = 0;
                    num = input.Length;
                    Console.WriteLine("You have entered \"" + input + "\", there are " + num + " characters in this word.");
                    invalid = false;
                }
            }         

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }

    }
}
