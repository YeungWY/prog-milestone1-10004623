﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
            static void Main(string[] args)
        {
            int CountDay = 0;

            for (int i = 1; i <= 31; i++)
            {
                DateTime date = new DateTime(2016, 8, i);
                string day;
                day = date.ToString("dddd");
                if (day == "Wednesday")
                {
                    CountDay++;
                }
            }

            Console.WriteLine("\nThere are " + CountDay + " Wednesdays in the month of August.");
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
