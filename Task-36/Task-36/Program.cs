﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            int counter = 6;
            for (int i = 0; i <= counter; i++)
            {
                if(i == counter)
                {
                    Console.WriteLine("Integer " + i + " is equal to the counter.");
                }
            }
            Console.WriteLine("\nThe loop is completed.");
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
